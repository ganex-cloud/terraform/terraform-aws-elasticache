module "elasticache-NAME-cache" {
  source             = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-elasticache.git?ref=master"
  name               = "NAME-cache"
  node_type          = "cache.t2.micro"
  engine             = "memcached"
  engine_version     = "1.5.10"
  port               = "11211"
  maintenance_window = "mon:06:00-mon:07:00"
  apply_immediately  = true

  family = "memcached1.5"

  subnet_ids = [
    "subnet-xxxxxx",
    "subnet-xxxxxx",
    "subnet-xxxxxx",
  ]

  vpc_id = "vpc-xxxxxx"

  source_security_group_id = [
    "sg-xxxxxx",
  ]
}
