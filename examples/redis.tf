module "elasticache-NAME-cache" {
  source                   = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-elasticache.git?ref=master"
  name                     = "NAME-cache"
  node_type                = "cache.t2.micro"
  engine                   = "redis"
  engine_version           = "4.0.10"
  port                     = "6379"
  maintenance_window       = "mon:06:00-mon:07:00"
  snapshot_window          = "04:10-05:10"
  snapshot_retention_limit = 7
  apply_immediately        = true
  family                   = "redis4.0"

  subnet_ids = [
    "subnet-xxxxxx",
    "subnet-xxxxxx",
    "subnet-xxxxxx",
  ]

  vpc_id = "vpc-xxxxxx"

  source_security_group_id = [
    "sg-xxxxxx",
  ]
}
